import { UIController } from "../../../Scripts/UIController";
import { UILayer } from "../../../Scripts/UIMgr";
import { MyInfoMgr, MyInfoEvent } from "./MyInfoMgr";
import { LabelComponent } from "cc";

export class UI_MyInfo extends UIController {
    constructor() {
        super('ui/MyInfo/UI_MyInfo', UILayer.HUD);
    }

    onCreated() {
        this.onButtonEvent('btn_close', () => {
            this.hide();
        });

        this.onButtonEvent('introduce/btn_refresh', () => {
            MyInfoMgr.inst.changeIntroduce();
        });

        //响应数据变化
        this.on(MyInfoEvent.INTRODUCE_CHANGED,()=>{
            this.refreshIntroduce();
        });

        //初次加载显示
        this.refreshIntroduce();
    }

    refreshIntroduce(){
        this.node.getChildByName('introduce').getComponent(LabelComponent).string = MyInfoMgr.inst.introduce;
    }
}